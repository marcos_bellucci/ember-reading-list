import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'article',
  classNames: ['book-group'],
  title: null,
  books: null,
  shelve: null,

  //filteredBooks updates when books lenght change
  filteredBooks: Ember.computed('books.[]', function() {
    let books = this.get('books'),
        shelve = this.get('shelve');

    return books.filter(
      //fat arrow creates a function
      b => b.get('shelves').indexOf(shelve) > -1
    );
  })

});
