import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

export default Router.map(function() {
  this.route('next');//search next template and next.js in routes
  this.route('to-buy');//search to-buy template and to-buy.js in routes
  this.route('history');
  this.route('search', function() {
    this.route('result', { path: ':term' });
  });
});
