import Ember from 'ember';
// Import JQuery
let $ = Ember.$;

function toEmber(val) {
  return Ember.Object.create({
    "title": val.title,
    "author": (val.author_name && val.author_name[0]) || 'Unknow author',
    "publicationDate": val.publish_date[0],
  });
}

var apiSearch = {
  books: term => {
    return $.getJSON('https://openlibrary.org/search.json?q='+term+'&limit=6&callback=')
    .then( data => data.docs);
  }
};

export default Ember.Route.extend({
  model: function(params) {
    var term = params.term.replace(" ", "+");

    return apiSearch.books(term).then( data => data.map( val => toEmber(val) ).sortBy('title') );
  }
});
